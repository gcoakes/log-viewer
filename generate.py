import os
import argparse
import logging
import jsonlog
import zipfile
from io import TextIOWrapper
from random import choice, randrange

jsonlog.basicConfig(level=logging.DEBUG)

msgs = """Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Praesent ac nulla at turpis efficitur interdum.
Suspendisse eget magna lobortis elit laoreet porttitor.
Ut ultricies urna et nisi vulputate ultricies.
Aliquam feugiat augue quis nibh fringilla accumsan.
Vestibulum ut urna vitae elit aliquam facilisis.
Vestibulum eget est ac dui placerat pretium in sed dui.
Duis scelerisque nibh at quam gravida egestas fermentum a arcu.
Integer dictum libero vitae varius malesuada.
Quisque sollicitudin odio at tempor dictum.
Maecenas ut orci condimentum, semper risus quis, iaculis mi.
Morbi mattis turpis at quam cursus auctor ut in justo.
Integer quis mauris vel urna auctor scelerisque.
Donec at magna lobortis, vehicula massa at, laoreet elit.
Curabitur varius libero eget lorem ullamcorper, at mollis justo hendrerit.
Nunc eu tortor vitae massa posuere dapibus eu a tellus.
Mauris non ligula at nisl mattis elementum.
Duis non velit ut lectus ullamcorper maximus sed blandit risus.
Morbi vitae nisl ac lorem accumsan rutrum quis vitae urna.
Sed eu ante ut turpis placerat scelerisque a at dolor.
Curabitur non lacus ut mauris imperdiet commodo.
Sed tincidunt lorem vel nisl elementum, in molestie sem facilisis.
Duis et mi feugiat, consectetur leo at, facilisis orci.
Morbi vitae justo imperdiet justo auctor luctus.""".split(
    "\n"
)

levels = [
    logging.DEBUG,
    logging.DEBUG,
    logging.DEBUG,
    logging.DEBUG,
    logging.DEBUG,
    logging.DEBUG,
    logging.DEBUG,
    logging.DEBUG,
    logging.DEBUG,
    logging.DEBUG,
    logging.DEBUG,
    logging.INFO,
    logging.INFO,
    logging.INFO,
    logging.INFO,
    logging.INFO,
    logging.INFO,
    logging.INFO,
    logging.WARNING,
    logging.ERROR,
    logging.CRITICAL,
]

extras = [
    {"foo": "bar"},
    {"foo": "blarg"},
    {"blarg": {"foo": "bar"}},
    {"something": [randrange(1, 1000) for _ in range(10)]},
    {"nested": {"something": [randrange(1, 1000) for _ in range(10)]}},
    {"links": ["example.bin"]},
    None,
    None,
    None,
]

names = [
    "foo",
    "foo.bar",
    "foo.blarg",
    "bar.foo",
]

parser = argparse.ArgumentParser()
parser.add_argument("--output", "-o", type=argparse.FileType("wb"), required=True)
args = parser.parse_args()

loggers = [jsonlog.getLogger(n) for n in names]

with zipfile.ZipFile(args.output, mode="w") as zip:
    with TextIOWrapper(zip.open("log.ndjson", "w")) as fh:
        jsonlog.basicConfig(stream=fh, level=logging.DEBUG)
        for n in range(1000):
            choice(loggers).log(choice(levels), choice(msgs), extra=choice(extras))
        try:
            raise ValueError("This is a test message.")
        except ValueError:
            choice(loggers).critical(choice(msgs), exc_info=True)
    with zip.open("example.bin", "w") as fh:
        for _ in range(1000):
            fh.write(os.urandom(1000))
