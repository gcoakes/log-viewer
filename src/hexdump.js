onmessage = (e) => {
  const reader = e.data.stream().getReader();
  let bytes = [];
  let lineNumber = 0;
  postMessage(`     ${
    Array.from({ length: 16 }, (v, k) => k)
      .map((x) => x.toString(16).padStart(2, "0"))
      .join(" ")
  } ASCII`);
  reader.read().then(({ done, value }) => {
    if (done) return;
    bytes = bytes.concat(Array.from(value));
    while (bytes.length >= 16) {
      postMessage(`\n${lineNumber.toString(16).padStart(4, "0")} ${
        bytes
          .splice(0, 16)
          .map((x) => x.toString(16).padStart(2, "0"))
          .join(" ")
      }`);
      lineNumber++;
    }
  });
};
