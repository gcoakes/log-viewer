import { Dexie } from "dexie";

class FileDatabase extends Dexie {
  files: Dexie.Table<Blob, string>;
  constructor() {
    super("FileDatabase");
    this.version(1).stores({
      files: "",
    });
    this.files = this.table("files");
  }
}

const db = new FileDatabase();

export async function putFile(name: string, blob: Blob) {
  return await db.files.put(blob, name);
}

export async function getFile(name: string): Promise<Blob> {
  return (
    (await db.files.get(name)) ||
    (await fetch(name).then((r) => (r.ok ? r.blob() : null)))
  );
}
